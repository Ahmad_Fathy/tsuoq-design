// JavaScript Document
jQuery(function ($) {
  "use strict";
  // ********* Revolution Slider *********
  //Home Three
  var revapi;
  revapi = jQuery("#rev_eight").revolution({
    sliderType: "standard",
    sliderLayout: "fullwidth",
    scrollbarDrag: "true",
    navigation: {
      arrows: {
        enable: true
      },
      touch: {
        touchenabled: "on",
        swipe_threshold: 75,
        swipe_min_touches: 1,
        swipe_direction: "horizontal",
        drag_block_vertical: false
      },
      bullets: {
        enable: true,
        hide_onmobile: false,
        style: "",
        hide_onleave: false,
        direction: "horizontal",
        h_align: "center",
        v_align: "bottom",
        h_offset: 20,
        v_offset: 20,
        space: 5,
        tmp: ''
      }
    },
    responsiveLevels: [1240, 1024, 778, 480],
    gridwidth: [1170, 992, 767, 480],
    gridheight: [630, 530, 470, 470],
    disableProgressBar: "on",
    spinner: "off",
  });

});