$(document).ready(function () {
  "use strict";
  // ********* Revolution Slider *********
  //Home Three
  var revapi;
  revapi = jQuery("#rev_eight").revolution({
    sliderType: "standard",
    sliderLayout: "fullwidth",
    scrollbarDrag: "true",
    navigation: {
      arrows: {
        enable: true
      },
      touch: {
        touchenabled: "on",
        swipe_threshold: 75,
        swipe_min_touches: 1,
        swipe_direction: "horizontal",
        drag_block_vertical: false
      },
      bullets: {
        enable: true,
        hide_onmobile: false,
        style: "",
        hide_onleave: false,
        direction: "horizontal",
        h_align: "center",
        v_align: "bottom",
        h_offset: 20,
        v_offset: 20,
        space: 5,
        tmp: ''
      }
    },
    responsiveLevels: [1240, 1024, 778, 480],
    gridwidth: [1170, 992, 767, 480],
    gridheight: [630, 530, 470, 470],
    disableProgressBar: "on",
    spinner: "off",
  });
  //Parallax
  $('.parallax').parallax("50%", 0.3);
  $('#owl-Professional, #owl-Medical, #owl-Commercial, #owl-Restaurants').owlCarousel({
    loop: true,
    dots: false,
    nav: true,
    navText: [
      '<a class="prev-blog"><i class="fa fa-angle-left"></i></a>',
      '<a class="next-blog"><i class="fa fa-angle-right"></i></a>'
    ],
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: false
      },
      300: {
        items: 2
      },
      600: {
        items: 3
      },
      768: {
        items: 4
      },
      900: {
        items: 5
      },
      1140: {
        items: 6
      }
    }
  });
  $('#owl-shop-section, #owl-commercial-section, #owl-medical-section, #owl-prof-section').owlCarousel({
    loop: true,
    dots: false,
    nav: true,
    navText: [
      '<a class="prev-blog"><i class="fa fa-angle-left"></i></a>',
      '<a class="next-blog"><i class="fa fa-angle-right"></i></a>'
    ],
    responsiveClass: true,
    responsive: {
      0: {
        items: 1
      },
      300: {
        items: 1
      },
      600: {
        items: 2
      },
      768: {
        items: 3
      },
      900: {
        items: 3
      },
      1140: {
        items: 3
      }
    }
  })
});