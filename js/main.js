(function ($) {
  "use strict";

  //Preloader
  Royal_Preloader.config({
    mode: "progress",
    background: "#ffffff",
    showProgress: true,
    showPercentage: false,
  });

  /*------ Sticky menu start ------*/
  var $window = $(window);
  $window.on("scroll", function () {
    var scroll = $window.scrollTop();
    if (scroll < 300) {
      $(".sticky").removeClass("is-sticky");
    } else {
      $(".sticky").addClass("is-sticky");
    }
  });
  /*------ Sticky menu end ------*/

  /*-------- Off Canvas Open close start--------*/
  $(".off-canvas-btn").on("click", function () {
    $("body").addClass("fix");
    $(".off-canvas-wrapper").addClass("open");
  });

  $(".btn-close-off-canvas,.off-canvas-overlay").on("click", function () {
    $("body").removeClass("fix");
    $(".off-canvas-wrapper").removeClass("open");
  });
  /*-------- Off Canvas Open close end--------*/

  /*------- offcanvas mobile menu start -------*/
  var $offCanvasNav = $(".mobile-menu"),
    $offCanvasNavSubMenu = $offCanvasNav.find(".dropdown");

  /*Add Toggle Button With Off Canvas Sub Menu*/
  $offCanvasNavSubMenu
    .parent()
    .prepend('<span class="menu-expand"><i></i></span>');

  /*Close Off Canvas Sub Menu*/
  $offCanvasNavSubMenu.slideUp();

  /* Scroll Animation */
  window.scrollReveal = new scrollReveal();

  // Show & Hide Advanced Search
  $("#showAdvancedSearch").click(function () {
    $("#advancedSearchRow").toggle();
    $(".show-text").toggle();
    $(".hide-text").toggle();
  });


  // Select
  $('select').each(function () {
    $(this).select2({
      theme: 'bootstrap4',
      width: $(this).data('width') ? $(this).data('width') : $(this).hasClass('w-100') ? '100%' : 'style',
      placeholder: $(this).data('placeholder'),
      allowClear: Boolean($(this).data('allow-clear')),
    });
  });

  //Parallax & fade on scroll
  function scrollBanner() {
    $(document).on("scroll", function () {
      var scrollPos = $(this).scrollTop();
      $(".parallax-fade-top").css({
        top: scrollPos / 2 + "px",
        opacity: 1 - scrollPos / 550,
      });
    });
  }
  scrollBanner();

  $(document).ready(function () {
    /* Scroll Too */
    $(".scroll").on("click", function (event) {
      event.preventDefault();
      var full_url = this.href;
      var parts = full_url.split("#");
      var trgt = parts[1];
      var target_offset = $("#" + trgt).offset();
      var target_top = target_offset.top - 68;
      $("html, body").animate({ scrollTop: target_top }, 800);
    });

    //Scroll back to top
    var offset = 300;
    var duration = 600;
    jQuery(window).on("scroll", function () {
      if (jQuery(this).scrollTop() > offset) {
        jQuery(".scroll-to-top").fadeIn(duration);
      } else {
        jQuery(".scroll-to-top").fadeOut(duration);
      }
    });
    jQuery(".scroll-to-top").on("click", function (event) {
      event.preventDefault();
      jQuery("html, body").animate({ scrollTop: 0 }, duration);
      return false;
    });

    //Parallax
    $(".parallax").parallax("50%", 0.3);
  });
})(jQuery);

$(document).ready(function () {
  $("a.favorite-heart").click(function () {
    $(this).toggleClass("active-red-color") &
      $(this).children('i.far').toggleClass('fas');
  });
});

$('.switch-list').click(function () {
  $('#grid-list').removeClass('grid').addClass('list');
});
$('.switch-grid').click(function () {
  $('#grid-list').removeClass('list').addClass('grid');
});
$('.grid-list-switcher a').click(function () {
  $(this).addClass('active').siblings('a').removeClass('active');
});
if ($('#grid-list').hasClass('list')) {
  alert('ahmad hi')
}

