/* -----------------------------------------------------------------------------

Zysk - Business & Finance HTML5 Template

File:           JS Core
Version:        1.0
Last change:    14/09/15 
Author:         Suelo

-------------------------------------------------------------------------------- */

"use strict";

var $html = $("html"),
  $body = $("body"),
  $bodyWrapper = $("#body-wrapper"),
  $pageLoader = $("#page-loader"),
  $searchToggle = $("#search-toggle"),
  $searchPopup = $("#search-popup");

/* Header Function */

var $header = $("#header"),
  $navBar = $("#nav-bar"),
  headerHeight = $header.height(),
  stickyBarrier = $(window).height() - $navBar.height() - 2,
  outBarrier = $header.height() * 2,
  scrolled = 0,
  $backToTop = $("#back-to-top");

window.setHeader = function () {
  scrolled = $(window).scrollTop();

  if (scrolled > headerHeight && !$header.hasClass("fixed")) {
    $header.addClass("fixed");
    if (!$header.hasClass("absolute")) {
      $bodyWrapper.css("padding-top", headerHeight + "px");
    }
  } else if (scrolled <= headerHeight && $header.hasClass("fixed")) {
    $header.removeClass("fixed");
    if (!$header.hasClass("absolute")) {
      $bodyWrapper.css("padding-top", 0);
    }
  }

  if (scrolled > outBarrier && !$header.hasClass("out")) {
    $header.addClass("out");
  } else if (scrolled <= outBarrier && $header.hasClass("out")) {
    $header.removeClass("out");
  }

  if (scrolled > stickyBarrier && !$header.hasClass("sticky")) {
    $header.addClass("sticky");
    $body.addClass("sticky-header");
  } else if (scrolled <= stickyBarrier && $header.hasClass("sticky")) {
    $header.removeClass("sticky");
    $body.removeClass("sticky-header");
  }

  if (scrolled > $(window).height() && !$backToTop.hasClass("visible")) {
    $backToTop.addClass("visible");
  } else if (scrolled <= $(window).height() && $backToTop.hasClass("visible")) {
    $backToTop.removeClass("visible");
  }
};

/* Main Functions */

var Zysk = {
  init: function () {
    this.Basic.init();
    this.Component.init();
  },
  Basic: {
    init: function () {
      this.mobileDetector();
      this.backgrounds();
      this.navigation();
      this.scroller();
    },
    mobileDetector: function () {
      var isMobile = {
        Android: function () {
          return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
          return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
          return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
          return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
          return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
          return (
            isMobile.Android() ||
            isMobile.BlackBerry() ||
            isMobile.iOS() ||
            isMobile.Opera() ||
            isMobile.Windows()
          );
        },
      };

      window.trueMobile = isMobile.any();
    },
    backgrounds: function () {
      // Image
      $(".bg-image, .post-wide .post-image").each(function () {
        var src = $(this).children("img").attr("src");
        $(this)
          .css("background-image", "url(" + src + ")")
          .children("img")
          .hide();
      });

      // Parallax
      $(".bg-parallax").parallax({
        zIndex: 0,
      });

      // Slideshow
      $(".bg-slideshow").owlCarousel({
        singleItem: true,
        autoPlay: 4000,
        pagination: false,
        navigation: false,
        navigationText: false,
        slideSpeed: 1500,
        transitionStyle: "fade",
        mouseDrag: false,
        touchDrag: false,
      });

      // Video
      var $bgVideo = $(".bg-video");
      if ($bgVideo) {
        $bgVideo.YTPlayer();
      }
      if (trueMobile && $bgVideo.hasClass("bg-video")) {
        $bgVideo.next(".bg-video-placeholder").show();
        $bgVideo.remove();
      }
    },
    animations: function () {
      // Animation - appear
      $(".animated").appear(function () {
        $(this).each(function () {
          var $target = $(this);
          var delay = $(this).data("animation-delay");
          setTimeout(function () {
            $target.addClass($target.data("animation")).addClass("visible");
          }, delay);
        });
      });
    },
    navigation: function () {
      var $nav = $("#nav-primary"),
        $toggleItem = $nav.find(".has-dropdown").children("a"),
        $navToggle = $("#mobile-nav-toggle");

      $navToggle.on("click", function () {
        $(this).toggleClass("active");
        $body.toggleClass("mobile-nav-open");
        $nav.slideToggle(300);

        return false;
      });

      $toggleItem.on("click", function () {
        if ($(window).width() < 1200) {
          $(this).next("ul").slideToggle(300);
        }

        return false;
      });

      window.setNavPrimary = function () {
        if ($(window).width() >= 1200) {
          $nav.show();
          $toggleItem.next("ul").each(function () {
            $(this).show();
          });
        }
        if ($(window).width() < 1200) {
          $nav.hide();
          $body.removeClass("mobile-nav-open");
          $navToggle.removeClass("active");
        }
      };
    },
    scroller: function () {
      var $header = $("#header");
      var headerHeight = $("#nav-bar").height();
      var $mobileNav = $("#mobile-nav");
      var $section = $("section", "#content");
      var scrollOffset = 0;
      scrollOffset = -headerHeight;
      var $scrollers = $("[data-local-scroll]");

      if ($body.hasClass("one-page"))
        $scrollers = $("#header, [data-local-scroll]");

      $scrollers.find("a").on("click", function () {
        $(this).blur();
      });

      $scrollers.localScroll({
        offset: scrollOffset,
        duration: 800,
        easing: "easeOutCubic",
      });

      var $menuItem = $("#nav-primary li > a");

      var checkMenuItem = function (id) {
        $menuItem.each(function () {
          var link = $(this).attr("href");
          if (id == link) $(this).addClass("active");
          else $(this).removeClass("active");
        });
      };
      $section.waypoint({
        handler: function (direction) {
          if (direction == "up") {
            var id = "#" + this.element.id;
            checkMenuItem(id);
          }
        },
        offset: function () {
          return -this.element.clientHeight + headerHeight;
        },
      });
      $section.waypoint({
        handler: function (direction) {
          if (direction == "down") {
            var id = "#" + this.element.id;
            checkMenuItem(id);
          }
        },
        offset: function () {
          return headerHeight + 1;
        },
      });
      $(window).resize(function () {
        setTimeout(function () {
          Waypoint.refreshAll();
        }, 600);
      });
    },
  },
  Component: {
    init: function () {
      this.backTotop();
      this.carousel();
      this.sidePanel();
      this.slider();
      this.videoBox();
    },
    backTotop: function () {
      if ($backToTop.length) {
        $backToTop.on("click", function () {
          $("html,body").animate(
            {
              scrollTop: 0,
            },
            1000,
            "easeInCubic"
          );
          return false;
        });
      }
    },
    carousel: function () {
      var $carousel = $(".carousel");

      if ($carousel.length) {
        $carousel.each(function () {
          var items,
            singleItem,
            autoPlay,
            transition,
            drag,
            stopOnHover,
            navigation,
            pagination;

          items = $(this).data("items");

          singleItem = $(this).data("single-item") === undefined ? false : true;
          autoPlay = $(this).data("autoplay");
          transition =
            $(this).data("transition") === undefined
              ? false
              : $(this).data("transition");
          pagination = $(this).data("pagination") === undefined ? false : true;
          navigation = $(this).data("navigation") === undefined ? false : true;
          drag = transition == "fade" ? false : true;
          stopOnHover =
            transition === "fade" ||
              pagination === false ||
              navigation === false
              ? false
              : true;

          $(this).owlCarousel({
            items: items,
            itemsDesktop: [1199, Math.ceil(items * 0.6)],
            itemsDesktopSmall: [991, Math.ceil(items * 0.5)],
            itemsTablet: [768, Math.ceil(items * 0.4)],
            itemsMobile: [479, Math.ceil(items * 0.2)],
            singleItem: singleItem,
            autoPlay: autoPlay,
            pagination: pagination,
            navigation: navigation,
            navigationText: false,
            slideSpeed: 800,
            stopOnHover: stopOnHover,
            transitionStyle: transition,
            mouseDrag: drag,
            touchDrag: drag,
            addClassActive: true,
          });
        });
      }
    },
    sidePanel: function () {
      if ($("#side-panel").length) {
        $('[data-toggle="side-panel"]').on("click", function () {
          $body.toggleClass("side-panel-open");
          return false;
        });

        $(".nav-panel a").on("click", function () {
          if ($(this).attr("href").indexOf(".html") == -1) {
            $body.removeClass("side-panel-open");
            return false;
          }
        });
      }
    },
    slider: function () {
      var $formSlider = $(".form-slider");

      if ($formSlider.length) {
        $(".form-slider").each(function () {
          var value = $(this).data("value"),
            min = $(this).data("min"),
            max = $(this).data("max"),
            targetInput = $(this).data("input");

          $(this).slider({
            range: "min",
            value: value,
            min: min,
            max: max,
            slide: function (event, ui) {
              $(targetInput).val(ui.value).trigger("change");
            },
          });
        });

        $(".slider-control").on("input", function () {
          $($(this).data("slider")).slider("value", $(this).val());
        });
      }
    },
  },
};

$(document).ready(function () {
  Zysk.init();
});

$(window).scroll(function () {
  setHeader();
  if ($searchPopup.length > 0 && $searchPopup.hasClass("visible")) {
    $searchToggle.removeClass("active");
    $searchPopup.removeClass("visible");
  }
});
